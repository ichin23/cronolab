# Cronolab

Um app de alunos, para alunos! 
[Acesse aqui](https://cronolab.vercel.app/)

## Apresentação

Quantas vezes já ficou perdido, sem saber quais atividades precisavam ser feitas e qual delas deveria ser priorizada?

Nosso app resolve esse problema, criando uma turma compartilhada onde os administradores adiciona as tarefas e todos os participantes da turma tem acesso a elas. Também possuí um filtro pra facilitar as buscas do usuário. As atividades podem ser filtradas por data, ou por matéria

<img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/home_list.png" width=200px alt="Home List"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/home_grid.png" width=200px alt="Home Grid"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/home_filtro.png" width=200px alt="Filtro de Atividade"/>

Além disso, cada usuário possui a opção de marcar a atividade como concluída, assim essa atividade vai para o fim da lista e ele não precisa mais se preocupar com ela.

<img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/concluida1.png" width=200px alt="Menu flutuante marcar concluída"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/concluida2.png" width=200px alt="Home com atividade concluída"/>

O usuário pode ver e editar suas infomrações facilmente

<img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/profile.png" width=200px alt="Tela Perfil"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/userData.png" width=200px alt="Informações do usuário"/>

Administradores tem acesso a opções como adicionar e editar atividade, e editar infomrações da turma

<img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/add_Dever.png" width=200px alt="Adicionar Dever"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/dever.png" width=200px alt="Tela dever"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/turma.png" width=200px alt="Editar turma"/> 


O app também conta com opções de login por email, e login facilitado com a conta do Google

<img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/login.png" width=200px alt="Tela Login"/> <img src="https://gitlab.com/ichin23/cronolab_assets/-/raw/main/cadastro.png" width=200px alt="Tela cadastro"/>


## Adicionais
- O servidor foi removido por questões de otimização do app. O app agora se comunica diretamente com o Firebase, para puxar os dados novos, e utiliza apenas dos dados salvos no SQLite, tornando a exibição das atividades mais rápida

## Futuros Incrementos
- Adição de um servidor para notificar os usuário quando uma tarefa não concluída está próxima de expirar
- Integração com o Google Agenda para facilitarn ainda mais ao usuário encontrar seus deveres